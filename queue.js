let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  if (collection.length === 0) {
    return undefined;
  }

  const updatedCollection = [];

  for (let i = 1; i < collection.length; i++) {
    updatedCollection[i - 1] = collection[i];
  }

  collection = updatedCollection;

  return collection;
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
